package luminous.db;

import luminous.model.Game;

public interface GameDAO {

    public String create();
    
    public Game get(String id);

    public void save(Game game);
}
