package luminous.db;

import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;
import luminous.model.Game;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class MorphiaGameDao implements GameDAO, AutoCloseable {
    
    private final Datastore datastore;

    public MorphiaGameDao(String host, int port, String dbName) {
        final Morphia morphia = new Morphia();
        morphia.mapPackage(Game.class.getPackage().getName());
        final MongoClient mongoClient = new MongoClient(host, port);
        datastore = morphia.createDatastore(mongoClient, dbName);
        datastore.ensureIndexes();
    }

    @Override
    public void close() throws Exception {
        datastore.getMongo().close();
    }

    @Override
    public String create() {
        final Game game = new Game();
        datastore.save(game);
        return game.getId().toString();
    }
    
    @Override
    public Game get(String id) {
        return datastore.get(Game.class, new ObjectId(id));
    }
    
    @Override
    public void save(Game game) {
//        datastore.save(game, WriteConcern.UNACKNOWLEDGED);
    }
}
