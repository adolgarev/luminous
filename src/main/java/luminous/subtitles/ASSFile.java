package luminous.subtitles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luminous.to.Dialogue;

public class ASSFile {

    enum ParseState {
        DONT_CARE,
        EVENTS,
        EVENTS_FORMAT
    }

    private final SubtitlesFileParseListener subtitlesFileParseListener;

    public ASSFile(SubtitlesFileParseListener subtitlesFileParseListener) throws IOException {
        this.subtitlesFileParseListener = subtitlesFileParseListener;
        String[] fileNames = {"Test.ass"};
        for (String fileName : fileNames) {
            parseFile(fileName);
        }
    }

    private void parseFile(String fileName) throws IOException {
        ParseState parseState = ParseState.DONT_CARE;
        Pattern sectionPattern = Pattern.compile("^\\[(.*)\\]$");
        Pattern linePattern = Pattern.compile("^(\\w+):\\s*(.*)$");
        String[] formatHeaders = null;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(fileName), StandardCharsets.UTF_8))) {
            String line;
            Integer lineNumber = 0;
            Integer dialogueNumber = 0;
            while ((line = in.readLine()) != null) {
                lineNumber++;
                Matcher sectionMatcher = sectionPattern.matcher(line);
                if (sectionMatcher.matches()) {
                    if (sectionMatcher.group(1).equals("Events")) {
                        parseState = ParseState.EVENTS;
                    } else {
                        parseState = ParseState.DONT_CARE;
                    }
                    continue;
                }
                Matcher lineMatcher = linePattern.matcher(line);
                switch (parseState) {
                    case EVENTS_FORMAT:
                        if (lineMatcher.matches() && lineMatcher.group(1).equals("Dialogue")) {
                            @SuppressWarnings("null")
                            String[] eventValues = lineMatcher.group(2).split("\\s*,\\s*", formatHeaders.length);
                            subtitlesFileParseListener.dialogue(Dialogue.fromASSLine(lineNumber, ++dialogueNumber, formatHeaders, eventValues, line));
                        }
                    case EVENTS:
                        if (lineMatcher.matches() && lineMatcher.group(1).equals("Format")) {
                            parseState = ParseState.EVENTS_FORMAT;
                            formatHeaders = lineMatcher.group(2).split("\\s*,\\s*");
                        }
                        break;
                }
            }
        }
    }
}
