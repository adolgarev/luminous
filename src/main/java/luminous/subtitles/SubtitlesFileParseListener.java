package luminous.subtitles;

import luminous.to.Dialogue;

public interface SubtitlesFileParseListener {
    public void dialogue(Dialogue dialogue);
}
