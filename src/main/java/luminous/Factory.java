package luminous;

import luminous.client.Serializer;
import luminous.client.Client;
import luminous.db.GameDAO;
import luminous.player.FixedIntervalTimeline;
import luminous.player.Timeline;
import luminous.player.FixedRatePlayer;
import luminous.player.Player;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import luminous.subtitles.ASSFile;
import luminous.client.JacksonSerializer;
import luminous.model.Game;
import luminous.db.MorphiaGameDao;
import luminous.client.UndertowHttpServer;

public class Factory {

    private final Timeline timeline;

    private final GameDAO gameDAO;

    private final Runnable server;
    
    private final ScheduledExecutorService scheduler;

    public Factory() {
        gameDAO = new MorphiaGameDao("localhost", 27017, "luminous");

        FixedIntervalTimeline fixedIntervalTimeline = new FixedIntervalTimeline();
        timeline = fixedIntervalTimeline;

        try {
            ASSFile assFile = new ASSFile(fixedIntervalTimeline);
        } catch (IOException ex) {
            Logger.getLogger(Factory.class.getName()).log(Level.SEVERE, null, ex);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);

        Serializer serializer = new JacksonSerializer(mapper);

        scheduler = Executors.newScheduledThreadPool(1);

        this.server = new UndertowHttpServer(9999, "0.0.0.0", gameDAO, this, serializer);        
    }

    public Runnable getServer() {
        return this.server;
    }

    public Player newPlayer(Client client, Game game) {
        return new FixedRatePlayer(client, game, timeline, gameDAO, 100L, scheduler);
    }
}
