package luminous.to;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.digest.DigestUtils;

public class Dialogue implements Comparable {

    private String id;
    
    private Integer lineNumber;
    
    private Integer dialogueNumber;

    private Integer layer;

    private Long startInMillis;

    private Long endInMillis;

    private String style;

    private String name;

    private String text;

    private String functionName;

    private Long functionArg;

    private String linkName;

    private Long linkArg;

    private Dialogue() {
    }

    public static Dialogue fromASSLine(Integer lineNumber, Integer dialogueNumber, String[] formatHeaders, String[] eventValues, String line) {
        Dialogue dialogue = new Dialogue();
        dialogue.lineNumber = lineNumber;
        dialogue.dialogueNumber = dialogueNumber;
        for (int i = 0; i < eventValues.length; i++) {
            switch (formatHeaders[i]) {
                case "Layer":
                    dialogue.layer = Integer.parseInt(eventValues[i]);
                    break;
                case "Start":
                    dialogue.startInMillis = eventTimeToMillis(eventValues[i]);
                    break;
                case "End":
                    dialogue.endInMillis = eventTimeToMillis(eventValues[i]);
                    break;
                case "Style":
                    dialogue.style = eventValues[i];
                    break;
                case "Name":
                    dialogue.name = eventValues[i];
                    break;
                case "Text":
                    processASSOverrides(dialogue, eventValues[i]);
                    break;
            }
        }
        dialogue.id = DigestUtils.sha1Hex(line);
        if (dialogue.layer == null
                || dialogue.startInMillis == null
                || dialogue.endInMillis == null
                || dialogue.style == null
                || dialogue.text == null) {
            throw new IllegalArgumentException();
        }
        return dialogue;
    }

    private static final Pattern FunctionPattern = Pattern.compile("\\{[^{}]*\\\\F([a-zA-Z]*)([^\\\\}]*)[^{}]*\\}");

    private static final Pattern LinkPattern = Pattern.compile("\\{[^{}]*\\\\L([a-zA-Z]*)([^\\\\}]*)[^{}]*\\}");
    
    private static final Pattern OverridePattern = Pattern.compile("\\{[^{}]*\\}");

    private static void processASSOverrides(Dialogue dialogue, String text) {
        Matcher functionMatcher = FunctionPattern.matcher(text);
        if (functionMatcher.find()) {
            String functionName = functionMatcher.group(1);
            if (functionName.length() == 0) {
                dialogue.functionName = "Goto";
            } else {
                dialogue.functionName = functionName;
            }
            try {
                dialogue.functionArg = Dialogue.eventTimeToMillis(functionMatcher.group(2));
            } catch (IllegalArgumentException illegalArgumentException) {
                try {
                    dialogue.functionArg = Long.valueOf(functionMatcher.group(2));
                } catch (NumberFormatException numberFormatException) {
                    dialogue.functionArg = null;
                }
            }
        }
        Matcher linkMatcher = LinkPattern.matcher(text);
        if (linkMatcher.find()) {
            String linkName = linkMatcher.group(1);
            if (linkName.length() == 0) {
                dialogue.linkName = "Goto";
            } else {
                dialogue.linkName = linkName;
            }
            try {
                dialogue.linkArg = Dialogue.eventTimeToMillis(linkMatcher.group(2));
            } catch (IllegalArgumentException illegalArgumentException) {
                try {
                    dialogue.linkArg = Long.valueOf(linkMatcher.group(2));
                } catch (NumberFormatException numberFormatException) {
                    dialogue.linkArg = null;
                }
            }
        }
        Matcher overrideMatcher = OverridePattern.matcher(text);
        dialogue.text = overrideMatcher.replaceAll("");
    }

    private static final Pattern EventTimePattern = Pattern.compile("^(\\d+):(\\d+):(\\d+).(\\d\\d)$");

    private static Long eventTimeToMillis(String timeAsString) {
        Matcher eventTimeMatcher = EventTimePattern.matcher(timeAsString);
        if (!eventTimeMatcher.matches()) {
            throw new IllegalArgumentException(timeAsString);
        }
        int hours = Integer.parseInt(eventTimeMatcher.group(1));
        int minutes = Integer.parseInt(eventTimeMatcher.group(2));
        int seconds = Integer.parseInt(eventTimeMatcher.group(3));
        int hundreds = Integer.parseInt(eventTimeMatcher.group(4));
        return (((hours * 24 + minutes) * 60 + seconds) * 100 + hundreds) * 10L;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Dialogue other = (Dialogue) obj;
        return Objects.equals(this.id, other.id);
    }
    
    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        final Dialogue other = (Dialogue) obj;
        return (int) (this.getStartInMillis() - other.getStartInMillis());
    }

    public String getId() {
        return id;
    }

    public Integer getLayer() {
        return layer;
    }

    public Long getStartInMillis() {
        return startInMillis;
    }

    public Long getEndInMillis() {
        return endInMillis;
    }

    public String getStyle() {
        return style;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public String getFunctionName() {
        return functionName;
    }

    public Long getFunctionArg() {
        return functionArg;
    }

    public String getLinkName() {
        return linkName;
    }

    public Long getLinkArg() {
        return linkArg;
    }
}
