package luminous.to;

public class DialogueRemoval {

    private final String id;

    public DialogueRemoval(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
