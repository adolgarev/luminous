package luminous.player;

public class PlayerCmd {
    public enum WhatToDo {
        OK, SKIP, GOTO
    }
    
    private final WhatToDo whatToDo;
    
    private final Long timeInMillis;
    
    public static final PlayerCmd OK = new PlayerCmd(WhatToDo.OK);

    public static final PlayerCmd SKIP = new PlayerCmd(WhatToDo.SKIP);

    private PlayerCmd(WhatToDo whatToDo, Long timeInMillis) {
        this.whatToDo = whatToDo;
        this.timeInMillis = timeInMillis;
    }

    private PlayerCmd(WhatToDo type) {
        this.whatToDo = type;
        this.timeInMillis = null;
    }

    public Long getTimeInMillis() {
        return timeInMillis;
    }

    public WhatToDo getWhatToDo() {
        return whatToDo;
    }
    
    public static PlayerCmd newGoto(Long timeInMillis) {
        return new PlayerCmd(WhatToDo.GOTO, timeInMillis);
    }
}
