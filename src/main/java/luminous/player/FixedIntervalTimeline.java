package luminous.player;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import luminous.subtitles.SubtitlesFileParseListener;
import luminous.to.Dialogue;

public class FixedIntervalTimeline implements SubtitlesFileParseListener, Timeline {

    public final Map<Long, SortedSet<Dialogue>> events;

    public final List<Dialogue> tooLongEvents;

    public final static long IntervalInMillis = 5000L;

    private final static long TooLongInMillis = 60000L;

    public FixedIntervalTimeline() {
        events = new HashMap<>();
        tooLongEvents = new LinkedList<>();
    }

    public static Long floorTimeToInterval(Long timeInMillist) {
        return timeInMillist / IntervalInMillis * IntervalInMillis;
    }

    @Override
    public void dialogue(Dialogue dialogue) {
        if (dialogue.getEndInMillis() - dialogue.getStartInMillis() > TooLongInMillis) {
            tooLongEvents.add(dialogue);
            return;
        }
        long start = floorTimeToInterval(dialogue.getStartInMillis());
        long end = floorTimeToInterval(dialogue.getEndInMillis());
        for (long i = start; i <= end; i += IntervalInMillis) {
            SortedSet<Dialogue> eventSet = events.get(i);
            if (eventSet == null) {
                eventSet = new TreeSet<>();
                events.put(i, eventSet);
            }
            eventSet.add(dialogue);
        }
    }

    @Override
    public Iterable<Dialogue> possibleDialogues(Long prevTimeInMillis, Long timeInMillis) {
        SortedSet<Dialogue> res = new TreeSet<>();
        for (long i = floorTimeToInterval(prevTimeInMillis); i <= floorTimeToInterval(timeInMillis); i += IntervalInMillis) {
            SortedSet<Dialogue> possibleDialogues = events.get(i);
            if (possibleDialogues != null) {
                for (Dialogue dialogue : possibleDialogues) {
                    if (dialogue.getStartInMillis() > prevTimeInMillis
                            && dialogue.getEndInMillis() < timeInMillis) {
                        res.add(dialogue);
                        continue;
                    } else if (dialogue.getStartInMillis() > timeInMillis
                            || dialogue.getEndInMillis() < timeInMillis) {
                        continue;
                    }
                    res.add(dialogue);
                }
            }
        }
        for (Dialogue dialogue : tooLongEvents) {
            if (dialogue.getStartInMillis() > timeInMillis
                    || dialogue.getEndInMillis() < timeInMillis) {
                continue;
            }
            res.add(dialogue);
        }
        return res;
    }

}
