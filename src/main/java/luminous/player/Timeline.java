package luminous.player;

import luminous.to.Dialogue;

public interface Timeline {
    public Iterable<Dialogue> possibleDialogues(Long prevTimeInMillis, Long timeInMillis);
}
