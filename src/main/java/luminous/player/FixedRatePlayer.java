package luminous.player;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import luminous.client.Client;
import luminous.db.GameDAO;
import luminous.model.Game;
import luminous.to.Dialogue;
import luminous.to.DialogueRemoval;

public class FixedRatePlayer implements Player, Runnable {

    private final ScheduledExecutorService scheduler;

    private final Timeline timeline;

    private final GameDAO gameDAO;

    private final Long eventPeriod;

    private Client client;

    private final ScheduledFuture eventHandle;

    private final Set<Dialogue> shownDialogues;

    private final Game game;

    public FixedRatePlayer(Client client, Game game, Timeline timeline, GameDAO gameDAO, Long eventPeriod, ScheduledExecutorService scheduler) {
        this.client = client;
        this.timeline = timeline;
        this.gameDAO = gameDAO;
        this.scheduler = scheduler;
        this.eventPeriod = eventPeriod;
        this.shownDialogues = new LinkedHashSet<>();
        this.game = game;
        eventHandle = scheduler.scheduleAtFixedRate(this, 10, eventPeriod, TimeUnit.MILLISECONDS);
    }

    @Override
    public synchronized Client getClient() {
        return client;
    }

    @Override
    public synchronized void setClient(Client client) {
        this.client = client;
    }

    @Override
    public synchronized void stop() {
        eventHandle.cancel(false);
        gameDAO.save(game);
    }

    @Override
    public synchronized void run() {
        Long prevTimeInMillis = game.getTimeInMillis();
        game.setTimeInMillis(prevTimeInMillis + eventPeriod);
        gameDAO.save(game);

        Iterator<Dialogue> shownDialoguesIterator = shownDialogues.iterator();
        while (shownDialoguesIterator.hasNext()) {
            Dialogue dialogue = shownDialoguesIterator.next();
            if (dialogue.getStartInMillis() > game.getTimeInMillis()
                    || dialogue.getEndInMillis() < game.getTimeInMillis()) {
                shownDialoguesIterator.remove();
                DialogueRemoval dialogueRemoval = new DialogueRemoval(dialogue.getId());
                client.send(dialogueRemoval);
            }
        }

        for (Dialogue dialogue : timeline.possibleDialogues(prevTimeInMillis, game.getTimeInMillis())) {
            if (dialogue.getStartInMillis() > prevTimeInMillis
                    && dialogue.getEndInMillis() < game.getTimeInMillis()) {
                if (dialogue.getFunctionName() != null) {
                    PlayerCmd res = game.invokeMethod(dialogue.getFunctionName(), dialogue.getFunctionArg());
                    if (res.getWhatToDo().equals(PlayerCmd.WhatToDo.GOTO)) {
                        // TODO: Goto
                        game.setTimeInMillis(res.getTimeInMillis());
                        break;
                    }
                }
                continue;
            }
            if (!shownDialogues.contains(dialogue)) {
                PlayerCmd res;
                if (dialogue.getFunctionName() != null) {
                    res = game.invokeMethod(dialogue.getFunctionName(), dialogue.getFunctionArg());
                } else {
                    res = PlayerCmd.OK;
                }
                switch (res.getWhatToDo()) {
                    case SKIP:
                        shownDialogues.add(dialogue);
                        break;
                    case GOTO:
                        // TODO: Goto
                        game.setTimeInMillis(res.getTimeInMillis());
                        break;
                    default:
                        shownDialogues.add(dialogue);
                        client.send(dialogue);
                }
            }
        }
    }
}
