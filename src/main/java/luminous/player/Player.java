package luminous.player;

import luminous.client.Client;

public interface Player {
    public Client getClient();

    public void setClient(Client client);

    void stop();
}
