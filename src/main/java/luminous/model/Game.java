package luminous.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import luminous.player.PlayerCmd;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity
public class Game {

    @Id
    private ObjectId id;

    private Long timeInMillis;

    public Game() {
        this.timeInMillis = 0L;
    }
    
    public PlayerCmd invokeMethod(String name, Long arg) {
        try {
            Method m = this.getClass().getDeclaredMethod(name, Long.class);
            return (PlayerCmd) m.invoke(this, arg);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ObjectId getId() {
        return id;
    }

    public Long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(Long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }
    
    public PlayerCmd Goto(Long timeInMillis) {
        return PlayerCmd.newGoto(timeInMillis);
    }

}
