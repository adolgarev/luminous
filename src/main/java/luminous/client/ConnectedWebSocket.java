package luminous.client;

import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectedWebSocket implements Client {

    private final WebSocketChannel channel;

    private final Serializer serializer;

    public ConnectedWebSocket(WebSocketChannel channel, Serializer serializer) {
        this.channel = channel;
        this.serializer = serializer;
    }

    public WebSocketChannel getChannel() {
        return channel;
    }

    @Override
    public void send(Object to) {
        if (channel.isOpen()) {
            WebSockets.sendText(serializer.toText(to), channel, null);
        }
    }

    @Override
    public void close() {
        try {
            channel.close();
        } catch (IOException ex) {
            Logger.getLogger(ConnectedWebSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
