package luminous.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import luminous.player.FixedRatePlayer;

public class JacksonSerializer implements Serializer {
    private final ObjectMapper mapper;

    public JacksonSerializer(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public String toText(Object to) {
        try {
            return mapper.writeValueAsString(to);
        } catch (JsonProcessingException ex) {
            Logger.getLogger(FixedRatePlayer.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalArgumentException(ex);
        }
    }
}
