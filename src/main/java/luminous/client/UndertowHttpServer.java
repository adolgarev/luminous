package luminous.client;

import static io.undertow.Handlers.resource;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.StreamSourceFrameChannel;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import java.io.IOException;
import luminous.db.GameDAO;
import luminous.player.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import luminous.model.Game;
import static io.undertow.Handlers.path;
import static io.undertow.Handlers.websocket;
import luminous.Factory;

public class UndertowHttpServer implements Runnable {

    private final int port;

    private final String host;

    private final GameDAO gameDAO;

    private final Factory factory;

    private final Map<String, Player> playerByGameId;

    private final Serializer serializer;

    public UndertowHttpServer(int port, String host, GameDAO gameDAO, Factory factory, Serializer serializer) {
        this.port = port;
        this.host = host;
        this.gameDAO = gameDAO;
        this.factory = factory;
        this.playerByGameId = new HashMap<>();
        this.serializer = serializer;
    }

    @Override
    public void run() {
        Undertow server = Undertow.builder()
                .addHttpListener(port, host)
                .setHandler(path()
                        .addPrefixPath("/Start", new HttpHandler() {
                            @Override
                            public void handleRequest(final HttpServerExchange exchange) throws Exception {
                                String id = gameDAO.create();
                                exchange.setResponseCode(StatusCodes.SEE_OTHER);
                                exchange.getResponseHeaders().put(Headers.LOCATION, "/theStorySoFar.html?id=" + id);
                            }
                        })
                        .addPrefixPath("/Channel", websocket(new MyWebSocketConnectionCallback()))
                        .addPrefixPath("/", resource(new ClassPathResourceManager(getClass().getClassLoader(), getClass().getPackage())).addWelcomeFiles("index.html")))
                .build();
        server.start();
    }

    private class MyWebSocketConnectionCallback implements WebSocketConnectionCallback {

        private final Pattern GameIdPattern = Pattern.compile("^.*\\?id=(.*)$");

        private String getGameIdByChannelUrl(WebSocketChannel channel) {
            Matcher matcher = GameIdPattern.matcher(channel.getUrl());
            if (!matcher.matches()) {
                throw new IllegalArgumentException();
            }
            return matcher.group(1);
        }

        @Override
        public void onConnect(WebSocketHttpExchange exchange, WebSocketChannel channel) {

            String id = getGameIdByChannelUrl(channel);
            ConnectedWebSocket connectedWebSocket = new ConnectedWebSocket(channel, serializer);
            synchronized (playerByGameId) {
                Player player = playerByGameId.get(id);
                if (player == null) {
                    Game game = gameDAO.get(id);
                    player = factory.newPlayer(connectedWebSocket, game);
                    playerByGameId.put(id, player);
                } else {
                    player.getClient().close();
                    player.setClient(connectedWebSocket);
                }
            }

            channel.getReceiveSetter().set(new AbstractReceiveListener() {
                @Override
                protected void onClose(WebSocketChannel webSocketChannel, StreamSourceFrameChannel channel) throws IOException {
                    String id = getGameIdByChannelUrl(webSocketChannel);
                    synchronized (playerByGameId) {
                        Player player = playerByGameId.get(id);
                        if (player != null) {
                            ConnectedWebSocket prevConnectedWebSocket = (ConnectedWebSocket) player.getClient();
                            if (prevConnectedWebSocket != null && prevConnectedWebSocket.getChannel() == webSocketChannel) {
                                player.stop();
                                prevConnectedWebSocket.close();
                            }
                        }
                    }
                }

                @Override
                protected void onFullTextMessage(WebSocketChannel channel, BufferedTextMessage message) {
                    WebSockets.sendText(message.getData(), channel, null);
                }
            });
            channel.resumeReceives();
        }
    }
}
