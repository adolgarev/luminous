package luminous.client;

public interface Serializer {

    public String toText(Object to);
}
