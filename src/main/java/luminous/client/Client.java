package luminous.client;

public interface Client {

    public void send(Object to);
    
    public void close();
}
